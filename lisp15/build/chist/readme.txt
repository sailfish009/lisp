This is the current state of Lisp-1.5. I have made one
pass over this and corrected many errors. I have not
verified comments or OCT/BCI. This will compile under
IBSYS with IBSFAP. It will produce a bootable deck on
PUA (cp0 under my sim). After booting deck, at first
halt, attach lisptest.job and start at address 174 octal.
This will setup the system and save an image on SYSTMP.
And produce a 2 card load deck on PUA. I am pretty sure
that this still contains errors. Once it is working 
correctly I will replace the IOS with IOCS from IBSYS
and allow it to be run under IBSYS.


