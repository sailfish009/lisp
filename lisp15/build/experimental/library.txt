       TAPE    SYSPIT,A1

       TAPE    SYSTAP,A2

       TAPE    SYSTMP,A3

       TAPE    SYSPOT,A4

       TAPE    SYSPPT,A5

       SIZE    14000Q,5000Q,4220Q,22400Q

       SET

REMPROP (DEFLIST EXPR)

ATTRIB (DEFLIST (EXPR (LAMBDA (J PROP) (PROG NIL

LOOP        (COND ((EQ NIL J) (RETURN T)))

            (REMPROP J PROP)

            (ATTRIB (CAAR J)

                    (LIST PROP (CADAR J)))

            (SETQ J (CDR J))

            (GO LOOP)))))

DEFLIST ((

  (DEFINE (LAMBDA (J) (DEFLIST J (QUOTE EXPR))))

) EXPR)

DEFINE ((

  (MEMBER (LAMBDA (X L)

            (PROG ()

              LOOP (COND ((NULL L) (RETURN NIL))

                         ((EQ (CAR L) X) (RETURN T)))

                   (SETQ L (CDR L))

                   (GO LOOP))))

  (PRINTPROP (LAMBDA (A)

    (PROG ()

      (SETQ A (CDR A))

      LOOP (COND ((NULL A) (RETURN T)))

           (PRINT (CAR A))

           (COND ((OR (EQ (CAR A) (QUOTE PNAME))

                      (EQ (CAR A) (QUOTE SUBR))

                      (EQ (CAR A) (QUOTE FSUBR)))

                  (PROG ()

                    (PRINT (QUOTE INTERNALFORMAT))

                    (SETQ A (CDR A)))))

           (SETQ A (CDR A))

           (GO LOOP))))

  (FLAG1 (LAMBDA (SYM FL)

           (PROG ()

             (COND ((NOT (MEMBER FL (CDR SYM)))

                    (RPLACD SYM (CONS FL (CDR SYM))))))))

  (FLAG (LAMBDA (L FL)

          (PROG ()

            LOOP (COND ((NULL L) (RETURN NIL)))

                 (FLAG1 (CAR L) FL)

                 (SETQ L (CDR L))

                 (GO LOOP))))

  (REM1FLAG (LAMBDA (L FL)

              (PROG ()

                LOOP (COND ((NULL L) (RETURN NIL))

                           ((EQ (CADR L) FL)

                            (PROG ()

                              (RPLACD L (CDDR L))

                              (RETURN NIL))))

                     (SETQ L (CDR L))

                     (GO LOOP))))

  (REMFLAG (LAMBDA (L FL)

             (PROG ()

               LOOP (COND ((NULL L) (RETURN NIL)))

                    (REM1FLAG (CAR L) FL)

                    (SETQ L (CDR L))

                    (GO LOOP))))

  (TRACE (LAMBDA (L) (FLAG L (QUOTE TRACE))))

  (UNTRACE (LAMBDA (L) (REMFLAG L (QUOTE TRACE))))

  (CSET (LAMBDA (SYM VAL)

          (PROG (TOSTORE)

            (REMPROP SYM (QUOTE APVAL))

            (SETQ TOSTORE (LIST VAL))

            (ATTRIB SYM (LIST (QUOTE APVAL) TOSTORE))

            (RETURN TOSTORE))))

  (OPDEFINE (LAMBDA (X)

              (PROG ()

                LOOP (COND ((NULL X) (RETURN NIL)))

                     (REMPROP (CAAR X) (QUOTE SYM))

                     (ATTRIB (CAAR X) (LIST (QUOTE SYM) (CADAR X)))

                     (SETQ X (CDR X))

                     (GO LOOP))))

))

DEFLIST ((

  (CONC (LAMBDA (ARGS ALIST)

          (MAPCON ARGS 

                  (FUNCTION 

                    (LAMBDA (L) 

                      (EVAL (CAR L) ALIST))))))

  (CSETQ (LAMBDA (ARGS ALIST)

           (CSET (CAR ARGS) (EVAL (CADR ARGS) ALIST))))
             
) FEXPR)

STOP)))   )))   )))   )))

       FIN      END OF LISP RUN       M948-1207 LEVIN          
