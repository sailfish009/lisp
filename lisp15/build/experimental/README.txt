
This is an attempt at getting the system to work using the card
reader as input, instead of MTA1.  Strangely, it seems to only read
every other line.

Attempts at using the card reader when booting from tape have not
yet succeeded.  The tape boot loader looks for correction cards and
then a binary transfer card.  I have not yet worked out the format
for this card.
