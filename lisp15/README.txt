
*** Running Lisp 1.5

Get yourself a copy of SIMH from http://simh.trailing-edge.com/

Unpack and compile yourself a copy of txt2bcd from the utils-1.1.8.tar.gz
package.  (originally from http://www.cozx.com/~dpitts/ibm7090.html)

Get yourself a copy of the Lisp 1.5 Programmers Manual from
http://www.softwarepreservation.org/projects/LISP/book
(The Primer and "The Programming Language Lisp" may also be
of interest.)

Run a lisp file with:
  i7094 lisptape.ini <lisp file>

Read the output in the sys.log file.

Sample lisp files:
  factorial.txt - Definition and an execution of the factorial function.
  funarg.txt - Some experiments with Lisp 1.5 FUNARGs.  See
               http://dspace.mit.edu/handle/1721.1/5854
  propcal.txt - A test program from the Lisp 1.5 Manual.  Interpreter
                for the Propositional Calculus.


*** Rebuilding Lisp 1.5

Building a bootable Lisp 1.5 tape is a two stage process.
  1) Assemble the source code into a binary suitable for direct loading 
     into SIMH.
  2) Run this interpreter, load some useful library functions into it,
     and have it dump a bootable tape image.

To assemble the source code, you need to compile a copy of asm7090
in build/asm7090 (originally from http://www.cozx.com/~dpitts/ibm7090.html)

The Lisp 1.5 source code is in build/chist/lisp.job (originally
from http://www.softwarepreservation.org/projects/LISP/lisp1.5/)
To assemble it, cd into build/ and type "make boottape/lisp.obj"

To create a bootable tape image, cd into build/boottape and type
"make syscore.tp"  This will run Lisp 1.5 in the i7094 emulator
with library.txt as input.  library.txt contains new definitions
of some Lisp 1.5 EXPRs and FEXPRs that are documented in the manual
but whose original definitions I do not have.

The resulting file, syscore.tp, can be copied over the sysboot.tp
file at the top level for use by the lisptape.ini SIMH initialization
file.

The whole thing can be done at once by typing "make realclean; make"
at the top level.


*** Extra Notes

The tape image "remembers" all of the tape assignmens that were made when it
was built.  That's why there are no tape assignments in the sample files.

The SIZE command for OVERLORD cannot be repeated.  It appears in
library.txt, but not in the samples.  To make any changes in the SIZE
settings, you will need to rebuild the bootable tape image.
